# Table of Contents

1.  [Train LinearRegression Model](#org0840114)
2.  [Define Variables](#org4fa4a7b)
    1.  [DB table column - HL<sub>PCT</sub>](#org9f2b2e5)
    2.  [DB table column - PCT<sub>change</sub>](#orge7984d7)
    3.  [Python Variable - X](#org0cb5c74)
    4.  [Python Variable - X<sub>lately</sub>](#orgad47d73)
    5.  [Python Variable - y](#org5908112)
    6.  [Python Variable - X<sub>train</sub>](#org7d7fea6)
    7.  [Python Variable - y<sub>train</sub>](#org8f2539b)
    8.  [Python Variable - X<sub>test</sub>](#org45a2768)
    9.  [Python Variable - y<sub>test</sub>](#orgc660158)
3.  [How LinearRegression Works](#org9b84834)
4.  [Save Model, using Pickle](#org5348d02)
5.  [Create](#org95b354e)

Quandl api key - 
Include pydoc
Include pip freeze output
Include try blocks


<a id="org0840114"></a>

# Train LinearRegression Model

Using Quandl, an API that provides stock price data. The python libarary returns a pandas dataframe.

Getting Data for Google stock prices from DATE - DATE. Will train model using LinearRegression, and use it to predict future stock prices.

Data:
Open:
High:
Low:
Close:
Volume:
Ex-Dividend:
Split Ratio:
Adj. Open:
Adj. High:
Adj. Low:
Adj. Close:
Adj. Volume:


<a id="org4fa4a7b"></a>

# Define Variables


<a id="org9f2b2e5"></a>

## DB table column - HL<sub>PCT</sub>


<a id="orge7984d7"></a>

## DB table column - PCT<sub>change</sub>

-   This is the percent change of closing price verses the opening opening price


$`PCT\_change = \frac{Closing\_Price - Opening\_Price}{Opening\_Price} \times 100`$


<a id="org0cb5c74"></a>

## Python Variable - X


<a id="orgad47d73"></a>

## Python Variable - X<sub>lately</sub>


<a id="org5908112"></a>

## Python Variable - y


<a id="org7d7fea6"></a>

## Python Variable - X<sub>train</sub>


<a id="org8f2539b"></a>

## Python Variable - y<sub>train</sub>


<a id="org45a2768"></a>

## Python Variable - X<sub>test</sub>


<a id="orgc660158"></a>

## Python Variable - y<sub>test</sub>


<a id="org9b84834"></a>

# How LinearRegression Works


<a id="org5348d02"></a>

# Save Model, using Pickle


<a id="org95b354e"></a>

# Create

